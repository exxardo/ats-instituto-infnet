import re
import requests
from bs4 import BeautifulSoup
from collections import Counter

palavras = 0
lista_palavras = []
aparece_uma_vez = []


url = "http://brasil.pyladies.com/about"

site = requests.get(url)
site.encoding = 'utf-8'
soup = BeautifulSoup(site.text, 'lxml')
for i in soup.html.body.article.find_all('div'):
    lista_palavras = [re.sub('\W+', '', palavra).lower()
    for palavra in i.text.split()]
palavras = len(lista_palavras)

localizar = ["ladies"]

corpo = []
texto = soup.find_all('p')
todo = soup.find_all('p')
corpo.append(texto)
vezes = []
for localizar in texto:
    vezes.append(corpo.count(texto))

dicio_todas_palavras = dict(Counter(lista_palavras))
for chave, valor in dicio_todas_palavras.items():
    if valor == 1:
        aparece_uma_vez.append(chave)

# A
print('Existem', len(lista_palavras), 'palavras no corpo da pagina')
print('Desse total,', len(aparece_uma_vez), 'aparecem apenas uma vez e são:')
print(aparece_uma_vez)

# B
print('\nA palavra "ladies" aparece', len(vezes), 'vezes no corpo da página.')
