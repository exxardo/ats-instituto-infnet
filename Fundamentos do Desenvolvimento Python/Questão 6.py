import pygame
import random


def desenha_quadrado():
    surface = pygame.display.set_mode((400, 300))
    color = (255, 255, 0)
    pos_x = random.randint(0, largura_tela - 30)
    pos_y = random.randint(0, altura_tela - 30)
    pygame.draw.rect(surface, color, pygame.Rect(pos_x, pos_y, 50, 50))
    pygame.display.flip()


# Iniciando a janela principal
largura_tela = 400
altura_tela = 300
tela = pygame.display.set_mode((largura_tela, altura_tela))
pygame.display.set_caption('Desenhar quadrado')
pygame.display.init()

# Cria relógio
clock = pygame.time.Clock()
# Contador de tempo
cont = 60

superficie_memoria = pygame.surface.Surface((730, 250))
superficie_disco = pygame.surface.Surface((730, 250))
superficie_cpu = pygame.surface.Surface((535, 250))

terminou = False
# Repetição para capturar eventos e atualizar tela
while not terminou:
    # Checar os eventos do mouse aqui:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            terminou = True
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
            desenha_quadrado()
        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            desenha_quadrado()

    # Atualiza o desenho na tela
    pygame.display.update()

    # 60 frames por segundo
    clock.tick(60)
    cont = cont + 1

# Finaliza a janela
pygame.display.quit()
