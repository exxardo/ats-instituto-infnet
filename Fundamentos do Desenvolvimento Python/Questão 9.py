import requests
from bs4 import BeautifulSoup

url = 'https://fgopassos.github.io/pagina_exemplo/estadosCentroOeste.html'

site = requests.get(url)
site.encoding = 'utf-8'
soup = BeautifulSoup(site.text, 'lxml')

estados = ['df', 'mt', 'go', 'ms']
estado_na_tabela = ''
escolhido = ''

tabela = soup.html.body.find('div', {'class': 'tabela'})
linhas = soup.html.body.article.find_all('div', {'class': 'linha'})

# A
print('Informações contidas na tabela', tabela.get_text())

# Tentei usar um break para sair do loop, mas não funcionou
fim_busca = False
while not fim_busca:
    estado_solicitado = input('Digite a UF desejada: ').upper()
    for linha in linhas:
        estado_na_tabela = linha.find_all('div', {'class': 'celula'})[0].text
        if estado_solicitado == estado_na_tabela:
            escolhido = linha.text
            fim_busca = True
        # O else está comentado porque a cada vez que o for executava uma busca ele printava
        # Remover depois
        '''else:
            print('Não há correspondência na tabela.')
            fim_busca = True'''

    if estado_solicitado != estado_na_tabela:
        print('Não há correspondência na tabela.')
        fim_busca = True

# B
print(escolhido)
