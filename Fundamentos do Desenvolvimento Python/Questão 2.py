#Função responsável por somar os termos ímpares
def somar_pares(total_termos):
    soma = 0
    # O 2 refer-re a por qual termos iremos começar, o total_termos refere-se até que termos iremos,
    # o + 1 sinaliza que vamos incluir na soma o termo final
    # e o 2 será de quantos em quantos termos iremos contar.
    for termo in range(2, total_termos + 1, 2):
        soma += termo
    return soma

#Receptor do dado do usuário
total_termos = int(input('Digite o número de termos: '))

print('A soma dos termos pares é', somar_pares(total_termos))