def pares(tupla):
    par = []
    for numero in tupla:
        if numero % 2 == 0:
            par.append(numero)
    return par


def impares(tupla):
    impar = []
    for numero in tupla:
        if numero % 2 != 0:
            impar.append(numero)
    return impar


tupla = (2, 3, 7, 15, 17, 18, 19, 25, 28, 37, 49, 53, 54, 64, 65, 67, 72, 80, 86, 87, 92, 96, 100)

print(f'Os números pares da tupla são:', pares(tupla))
print(f'Os números ímpares da tupla são:', impares(tupla))