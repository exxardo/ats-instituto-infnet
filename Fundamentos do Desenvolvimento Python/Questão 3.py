def potencia(base, expoente):
    # Poderia ser feito também com: base ** expoente. O Python faz isso sem a
    # necessidade de uma estrutura de repetição.
    resultado = 1
    verificador = 0
    while verificador < expoente:
        resultado = resultado * base
        verificador = verificador + 1

    return resultado


base = int(input('Insira o valor da base: '))
expoente = int(input('Insira o valor do expoente: '))

print(f'Teremos como resultado da potenciação:', potencia(base, expoente))