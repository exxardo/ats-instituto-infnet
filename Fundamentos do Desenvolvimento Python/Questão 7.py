import requests

url = "https://sites.google.com/site/dr2fundamentospython/arquivos/Winter_Olympics_Medals.csv"

csv = requests.get(url).text
linhas = csv.splitlines()
ouros = []

for linha in linhas:
    colunas = linha.split(',')
    if colunas[0] != 'Year' and int(colunas[0]) >= 2001:
        if colunas[4] == 'SWE' or colunas[4] == 'DEN' or colunas[4] == 'NOR':
            if colunas[2] == 'Curling' or colunas[2] == 'Skating' or colunas[2] == 'Skiing' or colunas[2] == \
                    'Ice Hockey':
                if colunas[7] == 'Gold':
                    ouros.append(colunas)

medalhas_pais = {
    'SWE': 0,
    'DEN': 0,
    'NOR': 0
}

for pais in ouros:
    if pais[4] == 'SWE':
        medalhas_pais['SWE'] += 1
    if pais[4] == 'DEN':
        medalhas_pais['DEN'] += 1
    if pais[4] == 'NOR':
        medalhas_pais['NOR'] += 1


print('O quadrado de medalhas é o seguinte:', medalhas_pais, '\nSendo Norway o país com mais medalhas,', medalhas_pais['NOR'], 'no total.')
