def calculo_de_comprometimento(renda_mensal, gasto_moradia, gasto_educacao, gasto_transporte):
        #calculo referente a moradia
    max_moradia_recomendado = (30/100) * renda_mensal
    gastos_totais_moradia = (gasto_moradia / renda_mensal) * 100
    
        #calculo referente a educação
    max_educacao_recomendado = (20/100) * renda_mensal
    gastos_totais_educacao = (gasto_educacao / renda_mensal) * 100
        #calculo referente a transporte
    max_transporte_recomendado = (15/100) * renda_mensal
    gastos_totais_transporte = (gasto_transporte / renda_mensal) * 100

#variavel vazia
    resultado = ''
    
#caminhos condicionais
    #condições para moradia
    if gasto_moradia > max_moradia_recomendado:
        print(f'\nDiagnóstico: \nSeus gastos totais com moradia comprometem {gastos_totais_moradia:.2f}% de sua renda total. O máximo recomendado é de 30%. Portanto, idealmente, o máximo de sua renda comprometida com moradia deveria ser de R$ {max_moradia_recomendado:.2f}.')
    else:
        print(f'\nDiagnóstico: \nSeus gastos totais com moradia comprometem {gastos_totais_moradia:.2f}% de sua renda total. O máximo recomendado é de 30%. Seus gastos estão dentro da margem recomendada.')
    
    #condições para educação
    if gasto_educacao > max_educacao_recomendado:
        print(f'Seus gastos totais com educação comprometem {gastos_totais_educacao:.2f}% de sua renda total. O máximo recomendado é de 20%. Portanto, idealmente, o máximo de sua renda comprometida com moradia deveria ser de R$ {max_educacao_recomendado:.2f}.')
    else:
        print(f'Seus gastos totais com educação comprometem {gastos_totais_educacao:.2f}% de sua renda total. O máximo recomendado é de 20%. Seus gastos estão dentro da margem recomendada.')
    
    #condições para transporte
    if gasto_transporte > max_transporte_recomendado:
        print(f'Seus gastos totais com transporte comprometem {gastos_totais_transporte:.2f}% de sua renda total. O máximo recomendado é de 15%. Portanto, idealmente, o máximo de sua renda comprometida com moradia deveria ser de R$ {max_transporte_recomendado:.2f}.')
    else:
        print(f'Seus gastos totais com transporte comprometem {gastos_totais_transporte:.2f}% de sua renda total. O máximo recomendado é de 15%. Seus gastos estão dentro da margem recomendada.')
#FIM DA FUNÇÃO QUE CALCULA O COMPROMETIMENTO

#solicta as informações
renda_mensal = float(input('Renda mensal total: R$ '))
gasto_moradia = float(input('Gastos totais com moradia: R$ '))
gasto_educacao = float(input('Gastos totais com educação: R$ '))
gasto_transporte = float(input('Gastos totais com transporte: R$ '))

#apresentação dos resultados com base nas solicitações
resultado = calculo_de_comprometimento(renda_mensal, gasto_moradia, gasto_educacao, gasto_transporte)
