#FUNÇÃO PARA CALCULAR O RENDIMENTO DA APLICAÇÃO
def calculo_rendimento(valor_inicial, porcentagem_por_periodo, aporte_por_periodo, total_periodos):
    rendimento = ((porcentagem_por_periodo / 100) * valor_inicial) + valor_inicial + aporte_por_periodo
    
    #LAÇO PARA SEGUNDA PARTE DO CALCULO
    for periodos in range(total_periodos):
        if periodos == 0:
            rendimento = ("%0.2f" % rendimento).replace(".", ",")
            print(f'\nApós {periodos + 1} períodos(s), o montante será de {rendimento}.')
            rendimento = (rendimento).replace(",", ".")
            rendimento = float(rendimento)
        else:
            rendimento = (rendimento * (porcentagem_por_periodo / 100)) +  rendimento + aporte_por_periodo
            rendimento = ("%0.2f" % rendimento).replace(".", ",")
            print(f'Após {periodos + 1} períodos(s), o montante será de {rendimento}.')
            rendimento = (rendimento).replace(",", ".")
            rendimento = float(rendimento)
    
#FIM DA FUNÇÃO CALCULAR_RENDIMENTO
 
#RECEPTOR DE INFORMAÇÕES
valor_inicial = float(input('Valor incial: R$ '))
porcentagem_por_periodo = float(input('Rendimento por período (%): '))
aporte_por_periodo = float(input('Aporte a cada período: R$ '))
total_periodos = int(input('Total de Perídos: '))

#CHAMADA DA FUNÇÃO
calculo_rendimento(valor_inicial, porcentagem_por_periodo, aporte_por_periodo, total_periodos)
