#FUNÇÃO PARA EXTRAÇÃO DOS DADOS DO ARQUIVO CSV.
def extrair_arquivo(caminho_arquivo):
    with open(caminho_arquivo, 'r', encoding='utf-8') as arquivo:
        conteudo = arquivo.read()
    conteudo = conteudo.splitlines()

    rotulos = conteudo [0]
    rotulos = rotulos.split(',')

    conteudo = conteudo[1:]
    dados = []
    for elemento in conteudo:
        elemento = elemento.split(',')
        dados.append(elemento)
    return rotulos, dados
#FIM DA FUNÇÃO EXTRAIR_ARQUIVO

#FUNÇÃO PARA RETORNAR O PIB, DE ACORDO COM O PAÍS E O ANO INFORMADO.
def pib_pais(ano, pais):
    rotulos, dados = extrair_arquivo('dados_pib.csv')
    indice_2013 = rotulos.index('2013')
    indice_2014 = rotulos.index('2014')
    indice_2015 = rotulos.index('2015')
    indice_2016 = rotulos.index('2016')
    indice_2017 = rotulos.index('2017')
    indice_2018 = rotulos.index('2018')
    indice_2019 = rotulos.index('2019')
    indice_2020 = rotulos.index('2020')
    indice_pais = rotulos.index('País')
    
    #LAÇO DE REPETIÇÃO COM CONDIÇÕES
    for elemento in dados:
        if elemento[indice_pais] == pais and ano == 2020:
            if True:
                pib = elemento[indice_2020]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2019:
            if True:
                pib = elemento[indice_2019]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2018:
            if True:
                pib = elemento[indice_2018]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2017:
            if True:
                pib = elemento[indice_2017]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2016:
            if True:
                pib = elemento[indice_2016]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2015:
            if True:
                pib = elemento[indice_2015]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2014:
            if True:
                pib = elemento[indice_2014]
                return pib
            else:
                pass
        elif elemento[indice_pais] == pais and ano == 2013:
            if True:
                pib = elemento[indice_2013]
                return pib
            else:
                pass
#FIM DA FUNÇÃO PIB_PAIS

#RECEPTORES DE DADOS COM CONDIÇÕES
pais = input("Informe um país: ").upper()
if pais == 'EUA' or  pais == 'CHINA' or pais == 'JAPÃO' or pais == 'ALEMANHA' or pais == 'REINO UNIDO' or pais == 'FRANÇA' or pais == 'BRASIL' or pais == 'ITÁLIA' or pais == 'ÍNDIA' or pais == 'RÚSSIA' or pais == 'CANADÁ' or pais == 'COREIA DO SUL' or pais == 'ESPANHA' or pais == 'MÉXICO' or pais == 'INDONÉSIA':
    pass
else:
    print('\033[1;33mPaís não disponível.\033[m')
    exit()
ano = int(input("Informe um ano entre 2013 e 2020: "))
if ano < 2013 or ano > 2020:
    print("\033[1;33mAno não disponível.\033[m")
    exit()
else:
    print(f'PIB {pais} em {ano}: US${pib_pais(ano, pais)} trilhão(ões).\n')
    
#FUNÇÃO PARA CALCULAR A VARIAÇÃO DO PIB
def calculo_variacao(eua, china, japao, alemanha, reinounido, franca, brasil, italia, india, russia, canada, coreiadosul, espanha, mexico, indonesia):
    rotulos, dados = extrair_arquivo('dados_pib.csv')
    indice_2013 = rotulos.index('2013')
    indice_2020 = rotulos.index('2020')
    indice_pais = rotulos.index('País')
    
    #CALCULO DA VARIAÇÃO DO PIB
    for elemento in dados:
        if elemento[indice_pais] == eua:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'EUA\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == china:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'China\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == japao:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Japão\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == alemanha:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Alemanha\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == reinounido:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Reino Unido\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == franca:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'França\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == brasil:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Brasil\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == italia:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Itália\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == india:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Índia\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == russia:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Rússia\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == canada:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Canadá\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == coreiadosul:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Coreia do Sul\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == espanha:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Espanha\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == mexico:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'México\t\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
        if elemento[indice_pais] == indonesia:
            pib_2020 = float(elemento[indice_2020])
            pib_2013 = float(elemento[indice_2013])
            variacao_pib = round((((pib_2020/pib_2013) * 100) - 100), 2)
            print(f'Indonésia\t\tVariação de {variacao_pib}% entre 2013 e 2020.')
#FIM DA FUNÇÃO CALCULAR_VARIACAO

#PRINT PARA EFEITO ESTÉTICO
print('Variação do PIB por país:')

#CHAMADA DA FUNÇÃO
calculo_variacao('EUA', 'CHINA', 'JAPÃO', 'ALEMANHA', 'REINO UNIDO', 'FRANÇA', 'BRASIL', 'ITÁLIA', 'ÍNDIA','RÚSSIA','CANADÁ','COREIA DO SUL','ESPANHA','MÉXICO','INDONÉSIA')


import matplotlib.pyplot as plt

#FUNÇÃO QUE GERA O GRÁFICO
def gerar_grafico(x, y):
    plt.plot(x, y)
    plt.xlabel('PERÍODO DE AVALIAÇÃO', size = 13)
    plt.ylabel('VALORES EM TRILHÕES DE DÓLARES', size = 13)
    plt.title(f'{pais_grafico}: EVOLUÇÃO DO PIB', size = 16)
    plt.title
    plt.show()
#FIM DA FUNÇÃO GERAR_GRAFICO

#FUNÇÃO REPONSÁVEL POR TRAZER OS DADOS PARA O GRÁFICO
def gerar_grafico_pib(pais_grafico):
    rotulos, dados = extrair_arquivo('dados_pib.csv')
    indice_2013 = rotulos.index('2013')
    indice_2014 = rotulos.index('2014')
    indice_2015 = rotulos.index('2015')
    indice_2016 = rotulos.index('2016')
    indice_2017 = rotulos.index('2017')
    indice_2018 = rotulos.index('2018')
    indice_2019 = rotulos.index('2019')
    indice_2020 = rotulos.index('2020')
    indice_pais = rotulos.index('País')
    anos = [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020]
    grafico_pib = []
    for elemento in dados:
        if elemento[indice_pais] == pais:
            grafico_pib.append(float(elemento[indice_2013]))
            grafico_pib.append(float(elemento[indice_2014]))
            grafico_pib.append(float(elemento[indice_2015]))
            grafico_pib.append(float(elemento[indice_2016]))
            grafico_pib.append(float(elemento[indice_2017]))
            grafico_pib.append(float(elemento[indice_2018]))
            grafico_pib.append(float(elemento[indice_2019]))
            grafico_pib.append(float(elemento[indice_2020]))
    gerar_grafico(anos, grafico_pib)
#FIM DA FUNÇÃO GERAR_GRAFICO_PIB
    
#SOLICITAÇÃO AO USUÁRIO
pais_grafico = input('\nQual país deseja gerar o gráfico?: ').upper()

#CHAMADA DA FUNÇÃO
gerar_grafico_pib(pais_grafico)
